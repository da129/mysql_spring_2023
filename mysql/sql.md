# SQL with MySql

### Thought Experiment
Part 1
* make some sort of record system to keep track of employees
* must include name, phone, title, age
Part 2
* create another record keeping system to keep track of what projects an employee works on

### What is a DataBase (DBMS - Database Management System)?
* Relational (Maria, PostGres, Mysql) vs Non Relational (Mongo)
* storage of data
* software, not a language

### What is SQL (Structured Query Language)
* it's technically a language (though this not always the case)
* its how we get data out of the DB
* used by relational databases


### Installation
* reserve an ubuntu VM at https://vcm.duke.edu/
* power on the VM and connect to it via ssh. example: `ssh myNetid@vcm-12345.vm.duke.edu`
* run `sudo apt install mysql-server` and follow the prompts to install mysql
* run `sudo mysql --host localhost -u root`, this will connect you to the mysql server as the root user


### CREATE A USER and database
* **NOTE:** It's not *ideal* to use `WITH GRANT OPTION` if we can avoid it, but we'll want it for the purposes of learning today. Alternate way to create a user listed below. 
* `SHOW DATABASES;`
* `create database development;`
* `SHOW DATABASES;`
* `SELECT User FROM mysql.user;`
* `CREATE USER 'netid'@'%' IDENTIFIED BY 'password';`
* `GRANT ALL PRIVILEGES ON development.* TO 'netid'@'%' WITH GRANT OPTION;`
* `GRANT FILE ON *.* to 'netid'@'%';`
* `FLUSH PRIVILEGES;`


### Create our first table and entry
create a table in your database
```SQL
create table animals(
  name text,
  diet text,
  tax_class text,
  lifespan int,
  endangered bool
);
```

populate the table with rows
```SQL
insert into animals(
  name, diet, tax_class, lifespan, endangered
)

values(
  'Red Panda', "omnivore", "mammal", 8, 1
);
```

Repeat these steps with more entries
Animals
```
+--------------------+-----------+-----------+----------+------------+
| name               | diet      | tax_class | lifespan | endangered |
+--------------------+-----------+-----------+----------+------------+
| Corgi              | omnivore  | mammal    |       15 |          0 |
| Nile Crocodile     | carnivore | reptile   |       90 |          0 |
| Hawaiian Monk Seal | carnivore | mammal    |       25 |          1 |
| Okapi              | herbivore | mammal    |       25 |          1 |
| Ibis               | carnivore | bird      |       18 |          0 |
| Red Panda          | omnivore  | mammal    |        8 |          1 |
+--------------------+-----------+-----------+----------+------------+
```


### Query existing data
`select * from animals;`
`select * from animals where name = 'Red Panda';`
`select * from animals where name like 'red panda';`
`select * from animals where name like 'red';`
`select * from animals where name like 'red%';`

`select * from animals where diet = 'carnivore';`
`select * from animals where diet != 'carnivore';`

`select * from animals where endangered is TRUE;`
`select * from animals where endangered is FALSE;`

`select * from animals where lifespan > 20;`
`select * from animals where lifespan < 20;`

### Add columns to existing table
`alter table animals add column (fun_fact text);`

### Delete a row
`delete from animals where id = 3;`

### Update a value
```SQL
update animals
set fun_fact = "Dog of the Queen!"
where name = "corgi";
```

### Populate from a dump file
* `wget https://gitlab.oit.duke.edu/da129/mysql_spring_2023/-/raw/master/mysql/dump.sql`
* `mysql -u da129 -p development < dump.sql`
* EXTRA: how did this file get created in the first place? https://dev.mysql.com/doc/refman/8.0/en/mysqldump.html


# Additional Thoughts
### Create a User without Grant Option
* `SHOW DATABASES;`

* `SELECT User FROM mysql.user;`

* `CREATE USER 'netid'@'%' IDENTIFIED BY 'password';`

* `create database development;`

* `GRANT ALL PRIVILEGES ON development.* TO 'netid'@'%' IDENTIFIED BY 'password' ;`


### (Optional/Advanced) Expose your database system
* https://bhargavamin.com/how-to-do/change-mysql-server-port-ubuntu/
* edit this file `/etc/mysql/mysql.conf.d/mysqld.cnf` (note this is the case for your duke VM at the time of the writing. Location of this file varies based on system)
* edit the following 
  * `port = 6606` note the non standard port for added security
  * `bind-address = 0.0.0.0`
  * `mysqlx-bind-address     = 0.0.0.0` 
* restart the mysql server `sudo service mysql restart`
* now you should be able to connect remotely using other tools
